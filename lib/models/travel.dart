class Travel {
  String name;
  String location;
  String url;
  String desc;
  List<String> featureds;

  Travel(this.name, this.location, this.url, this.desc, this.featureds);

  static List<Travel> generateTravelBlog() {
    return [
      Travel(
          'Place 1',
          'Place 1',
          'assets/images/top1.jpg',
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
          [
            'assets/images/top1.jpg',
            'assets/images/top2.jpg',
            'assets/images/top3.jpg',
            'assets/images/top4.jpg',
            'assets/images/top2.jpg',
            'assets/images/top1.jpg',
          ]),
      Travel(
          'Place 2',
          'Place 2',
          'assets/images/top2.jpg',
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
          [
            'assets/images/top2.jpg',
            'assets/images/top3.jpg',
            'assets/images/top4.jpg',
            'assets/images/top1.jpg',
          ]),
      Travel(
          'Place 3',
          'Place 3',
          'assets/images/top3.jpg',
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
          [
            'assets/images/top3.jpg',
            'assets/images/top2.jpg',
            'assets/images/top3.jpg',
            'assets/images/top4.jpg',
            'assets/images/top1.jpg',
          ]),
      Travel(
          'Place 4',
          'Place 4',
          'assets/images/top4.jpg',
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
          [
            'assets/images/top4.jpg',
            'assets/images/top3.jpg',
            'assets/images/top4.jpg',
            'assets/images/top1.jpg',
          ]),
    ];
  }

  static List<Travel> generateMostPopular() {
    return [
      Travel(
          'Place 5',
          'Place 5',
          'assets/images/bottom1.jpg',
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
          [
            'assets/images/top3.jpg',
            'assets/images/top4.jpg',
            'assets/images/top1.jpg',
          ]),
      Travel(
          'Place 6',
          'Place 6',
          'assets/images/bottom2.jpg',
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
          [
            'assets/images/top3.jpg',
            'assets/images/top4.jpg',
            'assets/images/top1.jpg',
          ]),
      Travel(
          'Place 7',
          'Place 7',
          'assets/images/bottom3.jpg',
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
          [
            'assets/images/top3.jpg',
            'assets/images/top4.jpg',
            'assets/images/top1.jpg',
          ]),
      Travel(
          'Place 8',
          'Place 8',
          'assets/images/bottom4.jpg',
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
          [
            'assets/images/top3.jpg',
            'assets/images/top4.jpg',
            'assets/images/top1.jpg',
          ]),
    ];
  }
}
