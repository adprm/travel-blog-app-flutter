import 'package:flutter/material.dart';

class CustomApppBar extends StatelessWidget {
  final IconData? leftIcon;
  final Color? leftIconColor;
  final IconData rightIcon;
  final Color rightIconColor;
  final Function? leftCallBack;
  final Function? rightCallBack;

  const CustomApppBar(this.leftIcon, this.leftIconColor, this.rightIcon,
      this.rightIconColor, this.leftCallBack, this.rightCallBack,
      {super.key});

  @override
  Widget build(BuildContext context) {
    return AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: Padding(
          padding: const EdgeInsets.only(left: 15),
          child: Icon(
            leftIcon,
            color: leftIconColor,
          ),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 15),
            child: Icon(
              rightIcon,
              color: rightIconColor,
            ),
          ),
        ]);
  }
}
