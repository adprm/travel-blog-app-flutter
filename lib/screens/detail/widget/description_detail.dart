import 'package:flutter/material.dart';

class DescriptionDetail extends StatelessWidget {
  final String desc;
  const DescriptionDetail(this.desc, {super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
      child: Text(
        desc,
        style: const TextStyle(color: Colors.grey, fontSize: 16, height: 1.5),
      ),
    );
  }
}
