import 'package:flutter/material.dart';

class FeaturedList extends StatelessWidget {
  final List featureds;

  const FeaturedList(this.featureds, {super.key});

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
        padding:
            const EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 20),
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) {
          return SizedBox(
            width: 120,
            child: Image.asset(
              featureds[index],
              fit: BoxFit.cover,
            ),
          );
        },
        separatorBuilder: (_, index) => const SizedBox(
              width: 10,
            ),
        itemCount: featureds.length);
  }
}
